﻿using UnityEngine;
using System.Collections;

public class MonsterSkillManager : SkillManager {
    public Animator myBrain;
    public bool cooling;
    public bool available;
    public bool canAfford;
    public GameObject spellObject;
    public bool isMage;

    protected override void Init()
    {
        me = gameObject.GetComponent<Actor>();
        myBrain = gameObject.GetComponent<Animator>();
        if (isMage)
        {
            currentSkill = new Fireball(0f, 3, 0f, spellObject, GameObject.FindGameObjectWithTag("Player"), this.gameObject);
        }
        else
        currentSkill = new Heal(6f, 5, 0f, this.gameObject);

        currentSkill.skillTime = currentSkill.skillCooldown;
    }

    protected override void SecondUpdate()
    {
        currentSkill.skillTime += Time.deltaTime;
        CheckSkillTime();
        CheckCost();
    }

    void CheckSkillTime()
    {
        if (currentSkill.skillTime >= currentSkill.skillCooldown)
        {
            available = true;
            if (myBrain.GetBool("toSkill") != available)
            {
                myBrain.SetBool("toSkill", available);
            }
        }
        else
        {
            available = false;
            if (myBrain.GetBool("toSkill") != available)
            {
                myBrain.SetBool("toSkill", available);
            }
        }
    }

    void CheckCost()
    {
        if (me.currMP >= currentSkill.skillCost)
        {
            canAfford = true;
            if (myBrain.GetBool("canCast") != canAfford)
            {
                myBrain.SetBool("canCast", canAfford);
            }
        }
        else
        {
            canAfford = false;
            if (myBrain.GetBool("canCast") != canAfford)
            {
                myBrain.SetBool("canCast", canAfford);
            }
        }
    }

    public override void RunSkill()
    {
        me.currMP -= currentSkill.skillCost;
        currentSkill.skillTime = 0f;
        currentSkill.SkillExecute();
    }

    public void EndSkill()
    {
        myBrain.SetTrigger("endSkill");
    }

    public void FacePlayer()
    {
        gameObject.transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
    }


}
