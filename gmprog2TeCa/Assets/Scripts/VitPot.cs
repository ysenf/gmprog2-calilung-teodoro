﻿using UnityEngine;
using System.Collections;

public class VitPot : Item
{

    public VitPot(int iID, string iName, string iDesc, int amt, string iIcon, GameObject targ)
    {
        myEffect = new Blessing(System.Type.GetType("VITBUFF"));
        owner = targ;
        itemID = iID;
        itemName = iName;
        itemDescription = iDesc;
        amount = amt;
        itemIcon = Resources.Load<Sprite>("" + iIcon);
    }
}
