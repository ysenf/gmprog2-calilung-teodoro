﻿using UnityEngine;
using System.Collections;

public class Player : Actor {
    public int currEXP;
    public int currLevel;
    public int expThreshold;
    public int maxStatGain;
    public string playerName;
    public GameObject skills;

    protected override void ActorStart()
    {
        currEXP = 0;
        currLevel = 1;
    }

    protected override void ActorUpdate()
    {

    }

    protected override void Death(GameObject attacker)
    {
        
    }

    public void LevelUp()
    {
        expThreshold += 50;
        currLevel += 1;
        VIT += (Random.Range(1, maxStatGain));
        STR += (Random.Range(1, maxStatGain));
        INT += (Random.Range(1, maxStatGain));
        SPR += (Random.Range(1, maxStatGain));
        SetHealthToMax();
       skills.GetComponent<SkillTable>().LearnSkill(currLevel);
    }

    public void ReceiveEXP(int amount)
    {
        currEXP += amount;
        if (currEXP >= expThreshold)
        {
            currEXP = 0;
            LevelUp();
        }
    }
}
