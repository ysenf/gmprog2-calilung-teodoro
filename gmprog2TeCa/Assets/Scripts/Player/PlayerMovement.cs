﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float stopDistance = 5f;
    private Vector3 moveDirection = Vector3.zero;
    private Vector3 destination;
    public EventSystem eventSystem;

    void Start()
    {
        destination = transform.position;
    }

    void Update()
    {
        CharacterController controller = GetComponent<CharacterController>();
        OrientActor();
        MovePlayer(controller);
    }

    void MovePlayer(CharacterController controller)
    {

        if (CheckArrival()==false)
        {
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            controller.SimpleMove(forward * Time.deltaTime * speed);
       }
        //moveDirection.y -= gravity * Time.deltaTime;
    }

    void OrientActor()
    {
        // create a container for raycast information, then cast a ray from the mouse to the world
        RaycastHit hit;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // if mouse is clicked and the ray hits something, set destination to the point that was hit
        if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 100.0f) && !eventSystem.IsPointerOverGameObject())
        {
            destination = hit.point;
        }
        // face destination, turning if there is an obstacle
        Vector3 dir = new Vector3(destination.x, gameObject.transform.position.y, destination.z);
        dir += AvoidObstacles();
        gameObject.transform.LookAt(dir);
    }

    bool CheckArrival()
    {
        Vector3 dir = destination - gameObject.transform.position;
        if (dir.magnitude <= stopDistance)
            return true;
        else
            return false;
    }

    public Vector3 AvoidObstacles()
    {
        Vector3 hitholder = Vector3.zero;
        RaycastHit nrminfo;
        RaycastHit sideInfo;
        RaycastHit otherSideInfo;

        if (Physics.Raycast(transform.position, transform.forward, out nrminfo, 2f))
        {
            if (nrminfo.transform != gameObject.transform)
            {
                hitholder = (nrminfo.normal * 2);
                Debug.DrawRay(transform.position, transform.forward * 2, Color.green);
            }
        }
        else if (Physics.Raycast(transform.position, transform.right, out sideInfo, 0.7f))
        {
            if (sideInfo.transform != gameObject.transform)
            {
                hitholder = (sideInfo.normal * 2);
                Debug.DrawRay(transform.position, transform.right * 0.7f, Color.green);
            }
        }
        else if (Physics.Raycast(transform.position, transform.right * -1, out otherSideInfo, 0.7f))
        {
            if (otherSideInfo.transform != gameObject.transform)
            {
                hitholder = (otherSideInfo.normal * 2);
                Debug.DrawRay(transform.position, transform.right * -0.7f, Color.green);
            }
        }
        else {
            hitholder = Vector3.zero;
            Debug.DrawRay(transform.position, transform.forward * 2, Color.red);
            Debug.DrawRay(transform.position, transform.right * 0.7f, Color.red);
            Debug.DrawRay(transform.position, transform.right * -0.7f, Color.red);
        }

        return hitholder;
    }
}
