﻿using UnityEngine;
using System.Collections;

public class PlayerNavigation : MonoBehaviour {

    private NavMeshAgent agent;
    public bool isPathing = false;
	public GameObject point;
	public bool cursorActive;
	public bool arrived;
    public float interactRadius;
    public PlayerInputManager myHands;

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, interactRadius);
    }

    void Start()
    {
        myHands = gameObject.GetComponent<PlayerInputManager>();
        agent = GetComponent<NavMeshAgent>();
		cursorActive = false;
		arrived = false;
    }

    void Update()
    {
		UpdatePathStatus();
        UpdateCursor();
        NPCCheck();
    }

    public void NPCCheck()
    {
        int thisMask = 1 << 8;
       Collider[] hitStuff = Physics.OverlapSphere(transform.position, interactRadius, thisMask);
        int i = 0;
        if (hitStuff.Length > 0)
        {
            while (i < hitStuff.Length)
            {
                if (hitStuff[i].CompareTag("shopNPC"))
                {
                    NotifyUINPC(true);
                    NotifyInputNPC(hitStuff[i].gameObject);
                    i++;
                }
            }
        }
        else {
            NotifyUINPC(false);
            NotifyInputNPC(null);
        }
    }

   public void ReceiveInputSetPath(RaycastHit hitVar)
    {
        //Debug.Log("calledme");
        //Debug.Log(hitVar.point);
        agent.SetDestination(hitVar.point);
        // agent.Resume();  
        SpawnCursor(hitVar);
    }

    void UpdatePathStatus()
    {
        isPathing = agent.hasPath;
    }

    void UpdateCursor()
    {
        if (agent.destination == agent.transform.position && cursorActive)
        {
            cursorActive = false;
			point.SetActive (false);
        }
    }

    void SpawnCursor(RaycastHit hit)
    {
        if (!cursorActive)
        {
            cursorActive = true;
			point.SetActive (true);
			point.transform.position = hit.point;
        }
        else {
			point.transform.position = hit.point;
        }
    }

   public void ClearTarget()
    {
        // Debug.Log("stopped");
        cursorActive = false;
        point.SetActive(false);
        agent.ResetPath();
    }

    public void NotifyUINPC(bool inRange)
    {
        Debug.Log(inRange);
        //tell UI that NPC is in range
    }

    public void NotifyInputNPC(GameObject NPC)
    {
        myHands.SetNPCTarget(NPC);
    }
}
