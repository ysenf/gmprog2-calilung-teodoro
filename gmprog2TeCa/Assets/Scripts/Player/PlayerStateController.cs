﻿/*using UnityEngine;
using System.Collections;

public class PlayerStateController : MonoBehaviour {
    public bool isMoving = false;
    public bool inCombat = false;
    public bool canAttack = false;
    public bool dontIdleMe = false;
    private Animator thisAnim;
    private PlayerNavigation thisNavigator;

	// Use this for initialization
	void Start () {
        thisAnim = this.gameObject.GetComponent<Animator>();
        thisNavigator = this.gameObject.GetComponent<PlayerNavigation>();
	}
	
	// Update is called once per frame
	void Update () {
       // UpdateCombatStates();
        UpdateMovementStates();
    }

    void UpdateMovementStates()
    {
        if(CheckIfMoving() != isMoving)
        {
            if (isMoving && !canAttack)
            {
                if(!dontIdleMe&&(!thisNavigator.isPathing))
                setIdle();
            }
            else if (!isMoving && !canAttack)
                setRun();

            isMoving = CheckIfMoving();
        }
    }

   public void UpdateCombatStates()
    {
        if (canAttack && inCombat)
        {
            thisAnim.SetTrigger("toAttack");
        }
    }

    bool CheckIfMoving()
    {
        if (thisNavigator.isPathing)
            return true;
        else
            return false;
    }

    void setIdle()
    {
        thisAnim.SetTrigger("toIdle");
    }

    void setRun()
    {
        thisAnim.SetTrigger("toRun");
    }

   public void setWait()
    {
        thisAnim.SetTrigger("toWait");
    }

}*/

using UnityEngine;
using System.Collections;

public class PlayerStateController : MonoBehaviour
{
    public bool isMoving = false;
    public bool inCombat = false;
    public bool canAttack = false;
    public bool dontIdleMe = false;
    private Animator thisAnim;
    private PlayerNavigation thisNavigator;

    // Use this for initialization
    void Start()
    {
        thisAnim = this.gameObject.GetComponent<Animator>();
        thisNavigator = this.gameObject.GetComponent<PlayerNavigation>();
    }

    // Update is called once per frame
    void Update()
    {
        // UpdateCombatStates();
        UpdateMovementStates();
    }

    void UpdateMovementStates()
    {
        if (CheckIfMoving() != isMoving)
        {
            if (isMoving && !canAttack)
            {
                if (!dontIdleMe)
                    setIdle();
            }
            else if (!isMoving && !canAttack)
                setRun();

            isMoving = CheckIfMoving();
        }
    }

    public void UpdateCombatStates()
    {
        if (canAttack && inCombat)
        {
            thisAnim.SetTrigger("toAttack");
        }
    }

    bool CheckIfMoving()
    {
        if (thisNavigator.isPathing)
            return true;
        else
            return false;
    }

    void setIdle()
    {
        thisAnim.SetTrigger("toIdle");
    }

    void setRun()
    {
        thisAnim.SetTrigger("toRun");
    }

    public void setWait()
    {
        thisAnim.SetTrigger("toWait");
    }

}

