﻿using UnityEngine;
using System.Collections;

public class MonsterAwareness : MonoBehaviour
{
    private float detectionRadius;
    private float leashRange;
    public Transform playertrans;
    public bool inRange = false;


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, detectionRadius);
    }

    void Start()
    {
        playertrans = GameObject.FindGameObjectWithTag("Player").gameObject.transform;
           detectionRadius = gameObject.GetComponent<MonsterStats>().detectionRange;
         leashRange = gameObject.GetComponent<MonsterStats>().leashRange;
    }

    void Update()
    {
        perceptionCheck();
       // Debug.Log(inRange);
    }

    public void perceptionCheck()
    {
        Vector3 distance = gameObject.transform.position - playertrans.position;
        if (distance.magnitude <= detectionRadius)
        {
            if (inRange == false)
            {
                gameObject.GetComponent<Animator>().SetBool("inRange", true);
            }
            inRange = true;
        }
        else if (inRange == true && distance.magnitude >= leashRange)
        {
            // Debug.Log("forgot triggered");
            gameObject.GetComponent<Animator>().SetBool("inRange", false);
            inRange = false;
        }
    }

    public void enterAware()
    {
      //  Debug.Log("seen");
        gameObject.GetComponent<Animator>().SetTrigger("toChase");
    }

    public void exitAware()
    {
     //  Debug.Log("forgotten");
        gameObject.GetComponent<Animator>().SetTrigger("toReturn");
    }
}