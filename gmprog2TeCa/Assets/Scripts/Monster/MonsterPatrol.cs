﻿using UnityEngine;
using System.Collections;

public class MonsterPatrol : MonsterBehavior
{
    public bool chase = false;
    public Vector3 patrolPoint;
    public Vector3 target;
    public NavMeshAgent thisAgent;
    public float maxPatrolRadius;
    public float patrolDetectDistance;
    public float pointSearchRange = 1f;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isPatrol", true);
        CheckHurt(animator);
        //Debug.Log("patrolling");
        maxPatrolRadius = animator.gameObject.GetComponent<MonsterStats>().maxPatrolRadius;
        patrolDetectDistance = animator.gameObject.GetComponent<MonsterStats>().patrolDetectDistance;
        thisAgent = animator.gameObject.GetComponent<NavMeshAgent>();
        patrolPoint = animator.gameObject.GetComponent<MonsterStats>().spawnPoint;
        thisAgent.SetDestination(GetNewDestination());
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CheckAggro(animator);
        CheckHurt(animator);
        if (CheckDestinationDistance(animator))
        {
           // Debug.Log("settingidle");
            setIdle(animator);
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isPatrol", false);
        CheckHurt(animator);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    public Vector3 GetNewDestination()
    {
        Vector3 tempTar = CheckLocation(GenerateRandomPatrolPoint(), pointSearchRange);
        target = tempTar;
        //Debug.Log(tempTar);
        return tempTar;
    }

    public bool CheckDestinationDistance(Animator animator)
    {
        Vector3 distance = target - animator.gameObject.transform.position;
     //   Debug.Log(distance.magnitude);
        if (distance.magnitude <= patrolDetectDistance)
        {
            return true;
        }
        else
            return false;
    }

    Vector3 CheckLocation(Vector3 checkPoint, float searchRange)
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(checkPoint, out hit, searchRange, NavMesh.AllAreas))
        {
            return hit.position;
        }
        return CheckLocation(GenerateRandomPatrolPoint(), pointSearchRange);
    }

    Vector3 GenerateRandomPatrolPoint()
    {
        float xPad = Random.Range((-1 * maxPatrolRadius), maxPatrolRadius);
        float zPad = Random.Range((-1 * maxPatrolRadius), maxPatrolRadius);
        Vector3 tempTar = patrolPoint;
        tempTar.x += xPad;
        tempTar.z += zPad;

        return tempTar;
    }

    void setIdle(Animator animator)
    {
        animator.SetTrigger("toIdle");
    }

}