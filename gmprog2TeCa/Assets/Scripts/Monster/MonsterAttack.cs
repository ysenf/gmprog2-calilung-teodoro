﻿using UnityEngine;
using System.Collections;


public class MonsterAttack : MonsterBehavior
{
    public float chaseRange = 3f;
    public GameObject player;
    public NavMeshAgent thisAgent;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        chaseRange = 6f;
        animator.SetBool("isAttack", true);
        CheckHurt(animator);
        thisAgent = animator.gameObject.GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag("Player");
        thisAgent.ResetPath();
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (CheckDistance(animator)){
            CheckAware(animator);
        }
        CheckHurt(animator);
    }

    bool CheckDistance(Animator anim)
    {
        Vector3 closestPoint = player.GetComponent<Collider>().ClosestPointOnBounds(anim.gameObject.transform.position);
        float distance = Vector3.Distance(closestPoint, anim.gameObject.transform.position);
        if (distance >= chaseRange)
            return true;
        else
            return false;
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isAttack", false);
        CheckHurt(animator);
    }
}
