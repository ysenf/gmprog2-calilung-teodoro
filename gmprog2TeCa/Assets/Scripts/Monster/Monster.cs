﻿using UnityEngine;
using System.Collections;
using System;

public class Monster : Actor {
    public MonsterType myType;
    public int EXPWorth;
    
   public enum MonsterType
    {
        Slime,
        Goblin,
        Pig,
        Mage,
        Monster
    };

    protected override void ActorStart()
    {

    }

    protected override void ActorUpdate()
    {

    }

    protected override void Death(GameObject attacker)
    {
        SendEXP(attacker);
        attacker.SendMessage("OnDeathTargUpdate", this.gameObject);
        gameObject.SendMessage("DropIt");
        attacker.SendMessage("UpdateKillQuest", myType);
        GameObject.Destroy(this.gameObject);
    }

    public void SendEXP(GameObject attacker)
    {
        attacker.SendMessage("ReceiveEXP", EXPWorth);
    }
}
