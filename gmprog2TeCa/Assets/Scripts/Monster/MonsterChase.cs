﻿using UnityEngine;
using System.Collections;

public class MonsterChase : MonsterBehavior
{
    public Vector3 target;
    public NavMeshAgent thisAgent;
    public GameObject player;
    public float attackRange;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isChase", true);
        CheckHurt(animator);
        // Debug.Log("chasing");
        attackRange = animator.gameObject.GetComponent<MonsterStats>().attackRange;
        player = GameObject.FindGameObjectWithTag("Player");
        thisAgent = animator.gameObject.GetComponent<NavMeshAgent>();
        getPlayerPosition();
        thisAgent.SetDestination(getPlayerPosition());
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isChase", false);
        CheckHurt(animator);
    }
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CheckHurt(animator);
        CheckLeash(animator);
        getPlayerPosition();
        thisAgent.SetDestination(getPlayerPosition());
        if (checkAttackRange(animator))
        {
            attackFunction(animator);
        }
        else {
            attackFail(animator);
        }


    }


    private Vector3 getPlayerPosition()
    {
        Vector3 posHolder = player.transform.position;
        target = posHolder;
        return posHolder;
    }

    protected virtual bool checkAttackRange(Animator animator)
    {
        Vector3 distance = player.transform.position - animator.transform.position;
        if ((Mathf.Abs(distance.magnitude)) <= attackRange)
        {
            return true;
        }
        else
            return false;
    }

    protected virtual void attackFunction(Animator animator)
    {
        animator.SetTrigger("toAttack");
    }

    protected virtual void attackFail(Animator animator)
    {
    }
}