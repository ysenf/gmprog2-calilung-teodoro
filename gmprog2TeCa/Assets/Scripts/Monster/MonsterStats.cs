﻿using UnityEngine;
using System.Collections;

public class MonsterStats : MonoBehaviour
{
    public float maxPatrolRadius;
    public float patrolDetectDistance;
    public float detectionRange;
    public float leashRange;
    public float attackRange;
    public Vector3 spawnPoint;

    void Start()
    {
        spawnPoint = transform.position;
    }

    void Update()
    {

    }
}