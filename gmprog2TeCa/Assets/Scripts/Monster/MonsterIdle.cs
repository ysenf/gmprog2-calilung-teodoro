﻿using UnityEngine;
using System.Collections;

public class MonsterIdle : MonsterBehavior
{

    public float idleMax = 5f;
    public float idleMin = 3f;
    private float idleTime;
    private float genIdle;
    
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("isIdle", true);
        CheckHurt(animator);
        idleTime = 0f;
        genIdle = Random.Range(idleMin, idleMax);
       // Debug.Log("idling");
	}


	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        CheckHurt(animator);
        CheckAggro(animator);
      //   Debug.Log("idling");
        idleTime += Time.deltaTime;
      //  Debug.Log(idleTime);
        if (idleTime >= genIdle)
        {
            UnIdle(animator);
            idleTime = 0f;
        }
	}

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isIdle", false);
        CheckHurt(animator);
    }

    private void UnIdle(Animator animator)
    {
        animator.SetTrigger("toPatrol");
    }
}
