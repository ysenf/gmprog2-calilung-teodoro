﻿using UnityEngine;
using System.Collections;

public class MonsterReturn : MonsterBehavior
{
    public Vector3 patrolPoint;
    public Vector3 target;
    public NavMeshAgent thisAgent;
    public GameObject player;
    public float maxPatrolRadius;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("isReturn", true);
        CheckHurt(animator);
        patrolPoint = animator.gameObject.GetComponent<MonsterStats>().spawnPoint;
        maxPatrolRadius = animator.gameObject.GetComponent<MonsterStats>().maxPatrolRadius;
        thisAgent = animator.gameObject.GetComponent<NavMeshAgent>();
        target = patrolPoint;
       // Debug.Log(target);
        thisAgent.SetDestination(patrolPoint);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        CheckAggro(animator);
        CheckHurt(animator);
        //  Debug.Log("leashing");
        Vector3 distance = target - animator.gameObject.transform.position;
        if (distance.magnitude > maxPatrolRadius)
            thisAgent.SetDestination(patrolPoint);
        else
            animator.SetTrigger("toPatrol");

    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        animator.SetBool("isReturn", false);
        CheckHurt(animator);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
    //
    //}


}