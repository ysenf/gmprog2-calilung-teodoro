﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SkillEffect : MonoBehaviour {
    public List<SkillEffect> myHolder;
    public AffectHolder myHoldersHolder;

    public SkillEffect()
    {

    }

    public virtual void Execute(GameObject target, List<SkillEffect> holder, AffectHolder MHH)
    {

    }

    public virtual void Callme()
    {

    }

}
