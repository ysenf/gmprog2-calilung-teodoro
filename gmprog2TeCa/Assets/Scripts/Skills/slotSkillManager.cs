﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class slotSkillManager : MonoBehaviour, IPointerDownHandler {

	public int slotNumber;
	SkillTable skillTable;
	Image skillIcon;
	Text skillName;
	Text skillCost;




	// Use this for initialization
	void Start () {
		skillTable = this.gameObject.GetComponentInParent <SkillTable> ();
		skillIcon = gameObject.transform.GetChild (0).GetComponent<Image> ();
		skillName = gameObject.transform.GetChild (1).GetComponent<Text> ();
		skillCost = gameObject.transform.GetChild (2).GetComponent<Text> ();
	
	}
	
	// Update is called once per frame
	void Update () {



		if (skillTable.skillTable [slotNumber].skillName != null) {
			skillIcon.enabled = true;
			skillIcon.sprite = skillTable.skillTable [slotNumber].skillIcon;
			skillName.enabled = true;
			skillName.text = skillTable.skillTable [slotNumber].skillName;
			skillCost.enabled = true;
			skillCost.text = "MP: " +skillTable.skillTable [slotNumber].skillCost;
		} 
		else {
			skillIcon.enabled = false;
			skillName.enabled = false;
			skillCost.enabled = false;
		}
	
	}

	public void OnPointerDown (PointerEventData eventData){
		skillTable.changeSkill (skillTable.skillTable [slotNumber]);
	}
}
