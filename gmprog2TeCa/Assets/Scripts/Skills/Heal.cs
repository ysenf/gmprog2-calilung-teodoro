﻿using UnityEngine;
using System.Collections;

public class Heal : TargetSkill {
    public int efficacy;
    private bool isItem;

    public Heal(float CD, int cost, float time, GameObject me)
    {
        this.skillName = "Heal";
        this.skillIcon = Resources.Load<Sprite>("heal");
        skillCooldown = CD;
        skillCost = cost;
        skillTime = time;
        myExecutor = me;
        isItem = false;
    }

    public Heal(int effect)
    {
        efficacy = effect;
        isItem = true;
    }

    public override void SkillExecute()
    {
        TriggerExecutor("toGround");
    }

    public override void SkillTrigger()
    {
        Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      //ShittySkillEffect();
        Actor me = myExecutor.GetComponent<Actor>(); 
        me.TakeDamage(me.MATK*-1, myExecutor);
    }

    public override void AsItem(GameObject target)
    {
        Actor me = target.GetComponent<Actor>();
        me.TakeDamage(efficacy*-1, target);
    }
}
