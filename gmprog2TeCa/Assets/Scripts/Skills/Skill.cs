﻿using UnityEngine;
using System.Collections;


public class Skill {

	public string skillName;
	public float skillCooldown;
	public int skillCost;
	public float skillTime;
	public Sprite skillIcon;
	public bool isCooldown;
    public GameObject myExecutor;
    public GameObject prefabEffect;
    public GameObject goEffect;

        
	public Skill(string sName,float sCooldown, int sCost, string sIcon){
		skillName = sName;
		skillCooldown = sCooldown;
		skillCost = sCost;
		skillIcon = Resources.Load<Sprite> ("" + sIcon);
	}

    public Skill()
    {

    }

    public virtual void SkillInit()
    {

    }

    public virtual void SkillResolve()
    {

    }

    public virtual void SkillExecute()
    {

    }

    public virtual void SkillTrigger()
    {

    }

    public virtual void ShittySkillEffect()
    {
        //goEffect = (GameObject)Instantiate(prefabEffect, GameObject.FindGameObjectWithTag("Player").transform.position, GameObject.FindGameObjectWithTag("Player").transform.rotation);
        //goEffect.GetComponent<ParticleSystem>().Play();
        //Destroy(goEffect, 1.5f);
    }

    protected void TriggerExecutor(string triggername)
    {
        myExecutor.GetComponent<Animator>().SetTrigger(triggername);
    }

    public virtual void AsItem(GameObject target)
    {

    }
}
