﻿using UnityEngine;
using System.Collections;

public class Fireball : HitboxSkill {
    GameObject enemy;

    public Fireball(float CD, int cost, float time, GameObject hbox, GameObject me, int boxID)
    {
        this.skillName = "Fireball";
        this.skillIcon = Resources.Load<Sprite>("firebolt");
        skillCooldown = CD;
        skillCost = cost;
        skillTime = time;
        myExecutor = me;
        hboxID = boxID;
    }

    public Fireball(float CD, int cost, float time, GameObject hbox, GameObject plr, GameObject me)
    {
        skillCooldown = CD;
        skillCost = cost;
        skillTime = time;
        myHitbox = hbox;
        enemy = plr;
        myExecutor = me;
    }

    public override void SkillExecute()
    {
        if (myExecutor.CompareTag("Foe")) {
        SetupHitbox(myExecutor.GetComponent<Actor>().MATK*9,
                    "Player",
                    myExecutor,
                    true);
        }
        else
        {
            enemy = myExecutor.GetComponent<PlayerTargeting>().lastTarget;
            SetupHitbox(myExecutor.GetComponent<Actor>().MATK * 9,
                        "Foe",
                        myExecutor);
        }

        TriggerExecutor("toGround");
    }


    public override void SkillTrigger()
    {
        GameObject fire = myExecutor.GetComponent<SkillManager>().ManualSpawn(myHitbox, myExecutor.GetComponent<Weapon>().spellSpawn.transform.position, myExecutor.transform.rotation);
        fire.GetComponent<ParticleSystem>().Play();
        Rigidbody rig = fire.GetComponent<Rigidbody>();
        Vector3 traj = Vector3.zero;
        if (enemy != null)
            traj = enemy.transform.position - myExecutor.transform.position;
        else
            traj = myExecutor.transform.forward;
        traj.y = 0;
        traj.Normalize();
        rig.velocity = traj *= 50;
    }

}
