﻿using UnityEngine;
using System.Collections;

public class Blessing : TargetSkill {
    public System.Type myType;


    public Blessing(System.Type type)
    {
        this.skillName = "Blessing";
        this.skillIcon = Resources.Load<Sprite>("BuffATK");
        myType = type;
    }

    public Blessing(System.Type type, int cost, float cooldown)
    {
        skillTime = 0f;
        skillCost = cost;
        this.skillName = "Blessing";
        this.skillIcon = Resources.Load<Sprite>("BuffATK");
        skillCooldown = cooldown;
        myType = type;
    }

    public override void SkillExecute()
    {
        //myEffect = myTarget.GetComponent<SkillEffect>();
        TriggerExecutor("toGround");
    }

    public override void SkillTrigger()
    {
        myExecutor.GetComponent<AffectHolder>().AddAffect(myType);
        ShittySkillEffect();
    }

    public override void AsItem(GameObject target)
    {
        target.GetComponent<AffectHolder>().AddAffect(myType);
    }
}
