﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class STRINTBUFF : SkillEffect {
    Actor ben;
    public float waittime;

    public override void Execute(GameObject target, List<SkillEffect> holder, AffectHolder MHH)
    {
        myHoldersHolder = MHH;
        this.gameObject.SetActive(true);
        myHolder = holder;
        ben = target.GetComponent<Actor>();
        ben.STR += 1;
        ben.INT += 1;
        ben.UpdateStats();
        StartCoroutine(callIt(6f, this));
    }


    IEnumerator callIt(float waittime, SkillEffect refaffect)
    {
        yield return new WaitForSeconds(waittime);
        ben.STR -= 1;
        ben.INT -= 1;
        ben.UpdateStats();
        myHolder.Remove(this);
    }
}
