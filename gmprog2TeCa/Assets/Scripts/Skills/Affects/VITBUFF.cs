﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VITBUFF : SkillEffect
{
    Actor ben;
    public float waittime;

    public override void Execute(GameObject target, List<SkillEffect> holder, AffectHolder MHH)
    {
        myHoldersHolder = MHH;
        this.gameObject.SetActive(true);
        myHolder = holder;
        ben = target.GetComponent<Actor>();
        ben.VIT += 1;
        ben.UpdateStats();
        StartCoroutine(callIt(6f, this));
    }


    IEnumerator callIt(float waittime, SkillEffect refaffect)
    {
        yield return new WaitForSeconds(waittime);
        ben.VIT -= 1;
        ben.UpdateStats();
        myHolder.Remove(this);
    }
}
