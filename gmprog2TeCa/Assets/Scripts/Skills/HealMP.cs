﻿using UnityEngine;
using System.Collections;

public class HealMP : TargetSkill
{
    public int efficacy;
    private bool isItem;

    public HealMP(float CD, int cost, float time, GameObject me)
    {
        skillCooldown = CD;
        skillCost = cost;
        skillTime = time;
        myExecutor = me;
        isItem = false;
    }

    public HealMP(int effect)
    {
        efficacy = effect;
        isItem = true;
    }

    public override void SkillExecute()
    {
        TriggerExecutor("toGround");
    }

    public override void SkillTrigger()
    {
        Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //ShittySkillEffect();
        Actor me = myExecutor.GetComponent<Actor>();
        me.TakeDamage(me.MATK * -1, myExecutor);
    }

    public override void AsItem(GameObject target)
    {
        Actor me = target.GetComponent<Actor>();
        me.TakeMPDamage(efficacy * -1);
    }
}
