﻿using UnityEngine;
using System.Collections;

public class HitboxSkill : Skill {

    public GameObject myTarget;
    public GameObject myHitbox;
    protected int hboxID;

    protected virtual void SetupHitbox(int damage, string targettype, GameObject me)
    {
        Hitbox myhbx = myExecutor.GetComponent<HitboxHolder>().myBoxes[hboxID].GetComponent<Hitbox>();
        myHitbox = myExecutor.GetComponent<HitboxHolder>().myBoxes[hboxID];
        myhbx.damage = damage;
        myhbx.targetType = targettype;
        myhbx.myParent = me;
    }

    protected virtual void SetupHitbox(int damage, string targettype, GameObject me, bool trash)
    {
        Hitbox myhbx = myHitbox.GetComponent<Hitbox>();
        myhbx.damage = damage;
        myhbx.targetType = targettype;
        myhbx.myParent = me;
    }




}
