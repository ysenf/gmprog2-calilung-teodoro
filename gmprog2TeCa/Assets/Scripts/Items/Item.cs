﻿using UnityEngine;
using System.Collections;

public class Item {

	public int itemID;
	public string itemName;
	public string itemDescription;
	public int amount;
	public Sprite itemIcon;
    public Skill myEffect;
    public GameObject owner;


		
	public Item(int iID,string iName, string iDesc, int amt, string iIcon){
		itemID = iID;
		itemName = iName;
		itemDescription = iDesc;
		amount = amt;
		itemIcon = Resources.Load<Sprite> ("" + iIcon);
	}

    public Item(){
    }


    public void ItemUse()
    {
        myEffect.AsItem(owner);
    }
}
