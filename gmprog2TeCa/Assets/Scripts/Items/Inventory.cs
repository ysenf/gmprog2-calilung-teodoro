﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class Inventory : MonoBehaviour {
    public GameObject me;
	public GameObject slot;
	public GameObject tooltip;
	public List<GameObject> slotInv = new List<GameObject>();
	public List<Item> inventory = new List<Item> ();
	public int windowX;
	public int windowY;
	public int fwindowX;
    public int rowCount = 3;
    public int columnCount = 4;
    public GameObject goldcount;
    public int gold;

	public void itemTooltipOn(Item item){
		tooltip.SetActive (true);
		tooltip.transform.GetChild (0).GetComponent<Text> ().text = item.itemDescription;
	}

	public void itemTooltipOff(){
		tooltip.SetActive (false);
	}

    void Awake()
    {
        this.gameObject.SetActive(true);
        AddGold(2);
        UpdateGold();

        int slotAmt = 0;

        fwindowX = windowX;
        for (int i = 0; i < rowCount; i++)
        {
            //Rows
            for (int j = 0; j < columnCount; j++)
            {
                //Columns
                GameObject slots = (GameObject)Instantiate(slot);
                slots.GetComponent<slotManager>().slotNumber = slotAmt;
                slotAmt++;
                slotInv.Add(slots);
                inventory.Add(new Item());
                slots.transform.parent = this.gameObject.transform;
                slots.name = "Slot" + (i + 1) + "." + (j + 1);
                slots.GetComponent<RectTransform>().localPosition = new Vector3(windowX, windowY, 0);
                Debug.Log(windowX);
                windowX += 55;
            }
            windowX = fwindowX;
            windowY -= 55;
        }
        this.gameObject.SetActive(true);
        me = GameObject.FindGameObjectWithTag("Player");
        this.gameObject.SetActive(false);
    }

	// Use this for initialization
	void Start () {
    }

	// Update is called once per frame
	void Update () {
        //me = GameObject.FindGameObjectWithTag("Player");
    }
	// Transfer Gold Related Shit To A Manager
    public void AddGold(int amount)
    {
        gold += amount;
        UpdateGold();
    }

    public void UpdateGold()
    {
        goldcount.GetComponent<Text>().text = "" + gold;
    }

	public void addItem(Item item){
		for (int i = 0; i < 12; i++) {
			if (inventory [i].itemName == null && !inInventory (item)) {
				inventory [i] = item;
                slotInv[i].GetComponent<slotManager>().item = item;
				return;
			}
			else if(inInventory(item)){
				if(inventory[i].itemName == item.itemName){
                    slotInv[i].GetComponent<slotManager>().item.amount++;
                    return;
				}
			}
		}
	}

	public bool inInventory(Item item){
		for (int i = 0; i < 12; i++) {
			if (item.itemName == inventory [i].itemName) {
				return true;
			}
		}
			return false;
	}

	public void searchItem(int itemID){
		switch (itemID) {
		case 1:
			addItem ((new HealthPotion (1, "Health Potion", "Restores 50HP", 1, "Potion1", 40, me)));
			break;
		case 2:
			addItem ((new MPPot (2, "MP Potion", "Restores 50MP", 1, "Potion2", 20, me)));
			break;
		case 3:
			addItem ((new VitPot (3, "VIT Potion", "Buffs VIT", 1, "Potion6", me)));
			break;
		default:
			break;
		}


	}
}
