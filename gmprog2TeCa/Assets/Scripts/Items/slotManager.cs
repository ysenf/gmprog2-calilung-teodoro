﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class slotManager : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
	public Item item;
	Image itemImage;
	public int slotNumber;
	Inventory inventory;
	Actor player;
	Text itemAmount;

	// Use this for initialization
	void Start ()
	{
		inventory = this.gameObject.GetComponentInParent < Inventory> ();
		itemImage = gameObject.transform.GetChild (0).GetComponent<Image> ();
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Actor> ();
		itemAmount = gameObject.transform.GetChild (1).GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (inventory.inventory[slotNumber].itemName != null) {
			itemImage.enabled = true;
			itemImage.sprite = inventory.inventory [slotNumber].itemIcon;
			itemAmount.enabled = true;
			itemAmount.text = ""+inventory.inventory[slotNumber].amount;
		} 
		else {
			itemImage.enabled = false;
			itemAmount.enabled = false;
		}

	
	}

	public void OnPointerExit (PointerEventData eventData){
		if (inventory.inventory [slotNumber].itemName != null) {
			inventory.itemTooltipOff ();
		}
	}

	public void OnPointerEnter (PointerEventData eventData){
		if (inventory.inventory [slotNumber].itemName != null) {
			inventory.itemTooltipOn ( inventory.inventory [slotNumber]);
		}
		
	}

	public void OnPointerDown (PointerEventData eventData){
        Debug.Log(item.myEffect.GetType());
        if(inventory.inventory[slotNumber].amount>0)
		item.ItemUse ();
        inventory.inventory [slotNumber].amount--;
		if (inventory.inventory [slotNumber].amount <= 0) {
			inventory.inventory [slotNumber] = new Item();
			inventory.itemTooltipOff ();
		}
	}
}

