﻿using UnityEngine;
using System.Collections;

public class Actor : MonoBehaviour {
    public int VIT;
    public int maxHP;
    public int currHP;
    public int STR;
	public int INT;
    public int MATK;
	public int SPR;
	public int maxMP;
    public int currMP;
	public float spRecovery;
    private SkillManager myManager;


	void Start () {
        myManager = null;
        myManager = gameObject.GetComponent<SkillManager>();
        SetHealthToMax();
        SetMagicToMax();
        UpdateStats();
        ActorStart();
	}
	

	void Update () {

		if (Time.time > spRecovery && currMP<maxMP) {
			currMP += SPR / 10;
			spRecovery = 1.0f + Time.time;
		}
        ActorUpdate();
	
	}


   protected void SetHealthToMax()
    {
        maxHP = VIT * 10;
        currHP = maxHP;
    }

    protected void SetMagicToMax()
    {
        maxMP = SPR * 5;
        currMP = maxMP;
    }

    public void UpdateStats()
    {
        MATK = INT * 5;
        maxHP = VIT * 10;
        if (currHP > maxHP)
            currHP = maxHP;
        if (currMP > maxMP)
            currMP = maxMP;
    }

    public int CalculateDamage()
    {
        return STR * 2;
    }

    public void TakeDamage(int damageReceived, GameObject attacker)
    {
        currHP = Mathf.Clamp(currHP-damageReceived, 0, maxHP);
        if (currHP <= 0)
        {
            Death(attacker);
        }

    }

    public void TakeMPDamage(int damageReceived)
    {
        Debug.Log("FUCK");
        currMP = Mathf.Clamp(currMP - damageReceived, 0, maxMP);
    }

    protected virtual void Death(GameObject attacker)
    {

    }

	public void useSkill(Skill skill){
        currMP -= skill.skillCost;
	}

    protected virtual void ActorUpdate()
    {

    }

    protected virtual void ActorStart()
    {

    }
}
