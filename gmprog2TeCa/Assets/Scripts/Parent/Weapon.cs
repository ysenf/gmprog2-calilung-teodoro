﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
    public bool isPlayer;
    public GameObject myWeapon;
    public bool isActive;
    public bool isHitting;
    public GameObject myTarget;
    public Actor myActor;
    public GameObject spellSpawn;

	void Start () {
        isHitting = false;
        isActive = false;
        myTarget = null;
        myActor = gameObject.GetComponent<Actor>();
        myWeapon.GetComponent<WeaponController>().isPlayer = isPlayer;
        if (!isPlayer)
        {
            myTarget = GameObject.FindGameObjectWithTag("Player");
        }
	}
	
	void Update () {
        CheckHit();
	}

    void CheckHit()
    {
        if (isHitting && isActive)
        {
            DealDamage(myActor.CalculateDamage(), myTarget);
            ResetWeaponHitbox();
            DisableWeapon();
        }
    }

    public void DealDamage(int value, GameObject victim)
    {
        victim.GetComponent<Actor>().TakeDamage(value, this.gameObject);
    }

    public void ActivateWeapon()
    {
        isActive = true;
    }

    public void DisableWeapon()
    {
        isActive = false;
    }

    public void ResetWeaponHitbox()
    {
        myWeapon.GetComponent<WeaponController>().ResetHitbox();
    }

    public void SetTarget(GameObject target)
    {
        myTarget = target;
        myWeapon.GetComponent<WeaponController>().target = myTarget;
    }

    public void ClearTarget()
    {
        myTarget = null;
    }
}
