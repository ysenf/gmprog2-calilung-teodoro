﻿using UnityEngine;
using System.Collections;

public class MPPot : Item
{
    public int efficacy;

    public MPPot(int iID, string iName, string iDesc, int amt, string iIcon, int efficacy, GameObject targ)
    {
        myEffect = new HealMP(efficacy);
        owner = targ;
        itemID = iID;
        itemName = iName;
        itemDescription = iDesc;
        amount = amt;
        itemIcon = Resources.Load<Sprite>("" + iIcon);
    }
}
