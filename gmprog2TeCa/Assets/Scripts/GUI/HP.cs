﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HP : MonoBehaviour {
	public GameObject player;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		this.GetComponent<Slider>().maxValue = player.GetComponent<Actor>().maxHP;
		this.GetComponent<Slider> ().value = player.GetComponent<Actor> ().currHP;


	}
}
