﻿using UnityEngine;
using System.Collections;

public class enemyHP : MonoBehaviour {
	public Vector3 temPos;
	public float offsetY = -2.0f;
	public float offsetX = 20.0f;
	public Vector3 HPBarPos;
	public Texture2D HPBar;
	public int currHP;
	public int maxHP;

	// Use this for initialization
	void Start () {
		offsetY = -2.0f;
		offsetX = 20.0f;
	
	}
	
	// Update is called once per frame
	void Update () {
		currHP = gameObject.GetComponent<Monster> ().currHP;
		maxHP = gameObject.GetComponent<Monster> ().maxHP;
	}

	void OnGUI() {
		HPBarPos = new Vector3 (gameObject.transform.position.x, gameObject.transform.position.y - offsetY, gameObject.transform.position.z);
		temPos = Camera.main.WorldToScreenPoint (HPBarPos);
		GUI.DrawTexture (new Rect (temPos.x - offsetX ,Screen.height - temPos.y, 89.25f*currHP/maxHP, 10.5f), HPBar, ScaleMode.ScaleAndCrop);
		//GUI.DrawTextureWithTexCoords(new Rect (temPos.x - offsetX ,Screen.height - temPos.y, 119.0f*currHP/maxHP , 14.0f), HPBar,new Rect (temPos.x - offsetX ,Screen.height - temPos.y, 119.0f , 14.0f), true );
	}
}
