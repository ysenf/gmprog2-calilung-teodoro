﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class StatsWindow : MonoBehaviour {
	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.GetChild (0).GetComponent<Text> ().text = "Name : "+player.GetComponent<Player> ().playerName;
		gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Level : " +player.GetComponent<Player> ().currLevel;
		gameObject.transform.GetChild (2).GetComponent<Text> ().text = "HP : " +player.GetComponent<Player> ().currHP + "/" + player.GetComponent<Actor> ().maxHP;
		gameObject.transform.GetChild (2).GetComponent<Text> ().text = "MP : " + player.GetComponent<Player> ().currMP + "/" + player.GetComponent<Actor> ().maxMP;
		gameObject.transform.GetChild (4).GetComponent<Text> ().text = "STR : " +player.GetComponent<Player> ().STR;
		gameObject.transform.GetChild (5).GetComponent<Text> ().text = "VIT : " +player.GetComponent<Player> ().VIT;
		gameObject.transform.GetChild (6).GetComponent<Text> ().text = "INT : "+player.GetComponent<Player> ().INT;
		gameObject.transform.GetChild (7).GetComponent<Text> ().text = "Attack : "+player.GetComponent<Player> ().STR * 2;
		gameObject.transform.GetChild (8).GetComponent<Text> ().text = "EXP : "+player.GetComponent<Player> ().currEXP;

	}
}
