﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPDisplay : MonoBehaviour {
	public GameObject player;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		this.GetComponent<Text> ().text = player.GetComponent<Actor>().currHP + " / " + player.GetComponent<Actor>().maxHP;


	}
}
