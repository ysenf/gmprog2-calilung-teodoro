﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class StatsButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler,IPointerExitHandler, IPointerUpHandler {

	Image statsImage;
	public GameObject statsWindow;

	// Use this for initialization
	void Start () {
		statsImage = gameObject.transform.GetChild (0).GetComponent<Image> ();
	
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}

	public void OnPointerExit (PointerEventData eventData){
		statsImage.enabled = false;
	}

	public void OnPointerEnter (PointerEventData eventData){
		statsImage.enabled = true;
	}

	public void OnPointerDown (PointerEventData eventData){
		statsImage.enabled = false;
		statsWindow.SetActive (!statsWindow.activeSelf);
	}

	public void OnPointerUp (PointerEventData eventData){
		statsImage.enabled = true;
	}
}
