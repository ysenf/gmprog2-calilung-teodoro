﻿using UnityEngine;
using System.Collections;

public class KillQuest : Quest {
    private int currObjective;
    private int maxObjective;
    private Monster.MonsterType myType;

    public KillQuest(int theNumber, int exp, int gold, Monster.MonsterType type, string name, QuestManager theMan)
    {
        goldReward = gold;
        expReward = exp;
        myType = type;
        myName = name;
        myManager = theMan;
        maxObjective = theNumber;
        currObjective = 0;
    }

    public override bool CompareObjective(Monster.MonsterType recType)
    {
        if (recType == myType)
            return true;
        else
            return false;
    }

    public override void UpdateObjective(Monster.MonsterType recType)
    {
        /* if (((myType != Monster.MonsterType.None) && (myType == recType)) || (myType == Monster.MonsterType.None))
          {
              currObjective++;
              if (CheckProgress())
              {
                  Debug.Log("WTFTWFWF");
                  Reward();
              }
          }*/

		if (myType != Monster.MonsterType.Monster)
        {
            if (myType == recType)
            {
                currObjective++;
            }
        }
		else if(myType == Monster.MonsterType.Monster)
        {
            currObjective++;
        }
    }

    public override bool CheckProgress()
    {
        if (currObjective >= maxObjective)
            return true;
        else
            return false;
    }

    public override void Reward()
    {
        myManager.GetComponent<InventoryManager>().AddGold(goldReward);
        myManager.GetComponent<Player>().ReceiveEXP(expReward);
    }

    public override void RemoveMe()
    {
        myManager.RemoveQuest(this);
    }

    public override string GiveProgress()
    {
        return "Killed " + currObjective + " out of " + maxObjective + ".";
    }


}
