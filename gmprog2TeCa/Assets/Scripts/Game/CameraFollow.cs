﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public GameObject player;
    public Vector3 offset;
    public float speed = 10f;
    private Vector3 target;

	// Use this for initialization
	void Start () {
        // offset = transform.position - player.transform.position;
        target = player.transform.position + offset;
       // Debug.Log(transform.position - player.transform.position+"AKLJSHDAKJSHDKAJSHD");
        SnapTo();
	}
	
	// Update is called once per frame
	void Update () {
        AcquireTarget();

        FollowTarget();
	}

   void FollowTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed*Time.deltaTime);
    }

    void AcquireTarget()
    {
        target = player.transform.position + offset;
    }

    void SnapTo()
    {
        transform.position = target;
    }
}
