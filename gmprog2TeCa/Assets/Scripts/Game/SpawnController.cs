﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour
{

    public GameObject Slime;
    public GameObject Goblin;
    public GameObject Pig;
    public List<GameObject> SpawnedMonsters = new List<GameObject>();
    public float spawnRate = 5.0f;
    private Vector3 spawnLocation;
    public float range = 1.0f;
    private float spawnTime;
    public int spawnCap = 10;




    // Use this for initialization
    void Start()
    {
        spawnTime = Time.time + spawnRate;

    }

    // Update is called once per frame
    void Update()
    {
        //Get Random Coordinates
		spawnLocation = new Vector3(Random.Range(-55, 55), Random.Range(1,7), Random.Range(-55, 55));
        //Check if coordinate is valid
        if (Time.time > spawnTime)
        {
            while (!CheckLocation(spawnLocation, range, out spawnLocation))
            {
            }
            if (CheckLocation(spawnLocation, range, out spawnLocation) && SpawnedMonsters.Count <= spawnCap)
            {
                SpawnMonster(Random.Range(1, 4));
                spawnTime = Time.time + spawnRate;
            }
        }


    }

    bool CheckLocation(Vector3 spawnPoint, float range, out Vector3 finalPoint)
    {
        NavMeshHit hit;
        if (NavMesh.SamplePosition(spawnPoint, out hit, range, NavMesh.AllAreas))
        {
            finalPoint = hit.position;
            return true;
        }
        finalPoint = new Vector3(Random.Range(-55, 55), Random.Range(-1, 5), Random.Range(-55, 55));
        return false;
    }

    void SpawnMonster(int type)
    {
        switch (type)
        {
            case 1:
                GameObject slime = (GameObject)Instantiate(Slime, spawnLocation, transform.rotation);
                SpawnedMonsters.Add(slime);
                break;
            case 2:
                GameObject goblin = (GameObject)Instantiate(Goblin, spawnLocation, transform.rotation);
                SpawnedMonsters.Add(goblin);
                break;
            default:
                GameObject pig = (GameObject)Instantiate(Pig, spawnLocation, transform.rotation);
                SpawnedMonsters.Add(pig);
                break;
        }

    }



}