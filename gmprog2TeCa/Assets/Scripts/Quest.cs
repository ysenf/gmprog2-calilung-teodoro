﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Quest {

    protected int expReward;
    protected int goldReward;
    protected Item[] itemReward;
    protected QuestManager myManager;
    protected string myName;
    public string MyName
    {
        get
        {
            return myName;
        }
        set
        {
            myName = value;
        }
    }

    public virtual bool CompareObjective()
    {
        return false;
    }

    public virtual bool CompareObjective(Monster.MonsterType recType)
    {
        return false;
    }

    public virtual void RemoveMe()
    {

    }

    public virtual bool UpdateObjective()
    {
        return false;
    }


    public virtual void UpdateObjective(Monster.MonsterType typeRec)
    {
        
    }

    public virtual bool CheckProgress()
    {
        return false;
    }

    public virtual void Reward()
    {

    }

    public virtual string GiveProgress()
    {
        return "woob";
    }
}
