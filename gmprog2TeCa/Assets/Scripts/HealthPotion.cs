﻿using UnityEngine;
using System.Collections;

public class HealthPotion : Item {
    public int efficacy;

    public HealthPotion(int iID, string iName, string iDesc, int amt, string iIcon, int efficacy, GameObject targ)
    {
        myEffect = new Heal(efficacy);
        owner = targ;
        itemID = iID;
        itemName = iName;
        itemDescription = iDesc;
        amount = amt;
        itemIcon = Resources.Load<Sprite>("" + iIcon);
    }
}
