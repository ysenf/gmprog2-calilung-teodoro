﻿using UnityEngine;
using System.Collections;

public class GroundCrush : HitboxSkill {

    public GroundCrush(float cooldown, int cost)
    {
        this.skillTime = 0f;
        this.skillName = "Ground Crush";
        this.skillIcon = Resources.Load<Sprite>("groundcrush");
        this.skillCooldown = cooldown;
        this.skillCost = cost; 
    }

    public override void SkillExecute()
    {
        SetupHitbox(myExecutor.GetComponent<Actor>().MATK,
                    "Foe",
                    myExecutor);

        TriggerExecutor("toGround");
    }
        
    public override void SkillTrigger()
    {
        GameObject.Instantiate(myHitbox, myExecutor.transform.position, Quaternion.identity);
        ShittySkillEffect();
    }
}
