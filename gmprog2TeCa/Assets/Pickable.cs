﻿using UnityEngine;
using System.Collections;

public class Pickable : MonoBehaviour {
    public bool isGold;
    public int value;
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Player")) {
			PickUp (other);
		}
	}

	public void PickUp(Collider player){
        if (isGold)
        {
            player.GetComponent<InventoryManager>().AddGold(value);

            GameObject.Destroy(this.gameObject);
        }
        else {
            player.GetComponent<InventoryManager>().inventory.GetComponent<Inventory>().searchItem(1);
            GameObject.Destroy(this.gameObject);
        }
	}
}
