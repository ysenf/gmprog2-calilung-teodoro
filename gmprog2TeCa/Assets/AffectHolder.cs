﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AffectHolder : MonoBehaviour {
    public List<SkillEffect> myAffects;

    void Start()
    {
        myAffects = new List<SkillEffect>();
    }

    public void AddAffect(System.Type type)
    {
        // newAff.gameObject.SetActive(true);
        Component var = gameObject.AddComponent(type);
        SkillEffect[] currskills = gameObject.GetComponents<SkillEffect>();
        int i = 0;
        while (i < currskills.Length)
        {
            if (currskills[i].GetInstanceID() == var.GetInstanceID())
            {
                myAffects.Add(currskills[i]);
                myAffects[myAffects.IndexOf(currskills[i])].Execute(this.gameObject, myAffects, this);
            }
            i++;
        }
    }

    public void Apply(float waittime, SkillEffect refaffect)
    {
        StartCoroutine(callIt(waittime, refaffect));
    }

    IEnumerator callIt (float waittime, SkillEffect refaffect)
    {
        yield return new WaitForSeconds(waittime);
        refaffect.Callme();

    }
}
