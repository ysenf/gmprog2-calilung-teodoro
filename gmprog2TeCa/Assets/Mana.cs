﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Mana : MonoBehaviour {
	public GameObject player;


	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		this.GetComponent<Slider> ().maxValue = player.GetComponent<Actor> ().maxMP;
		//Debug.Log (player.GetComponent < Actor> ().maxMP);
		this.GetComponent<Slider> ().value = player.GetComponent<Actor> ().currMP;
		//Debug.Log (player.GetComponent < Actor> ().currMP);

	}
}
