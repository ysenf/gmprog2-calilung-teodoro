﻿using UnityEngine;
using System.Collections;

public class ShopManager : MonoBehaviour {
	public GameObject player;
	public GameObject ShopUI;
	public GameObject inventory;
    public GameObject interactText;
    public bool isShopped;

	// Use this for initialization
	void Start () {
        isShopped = false;
        if (ShopUI.activeSelf)
        {
            ShopUI.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (!player.GetComponent<PlayerInputManager>().NPCtarget)
        {
            if (ShopUI.activeSelf)
            {
                ShopUI.SetActive(false);
            }
        }
    }

	public void ShopMethod(int itemID)
    {
		switch (itemID) {
		case 1:
			if (inventory.GetComponent<Inventory> ().gold >= 50 * itemID) {
				inventory.GetComponent<Inventory> ().AddGold (-(50 * itemID));
				inventory.GetComponent<Inventory> ().searchItem (itemID);
				break;
			} else
				break;
		case 2:
			if (inventory.GetComponent<Inventory> ().gold >= 50 * itemID) {
				inventory.GetComponent<Inventory> ().AddGold (-(50 * itemID));
				inventory.GetComponent<Inventory> ().searchItem (itemID);
				break;
			} else
				break;
		case 3:
			if (inventory.GetComponent<Inventory> ().gold >= 50 * itemID) {
				inventory.GetComponent<Inventory> ().AddGold (-(50 * itemID));
				inventory.GetComponent<Inventory> ().searchItem (itemID);
				break;
				} else
					break;
		default:
			break;
		}

		

    }

    public void Interacted()
    {
        isShopped = !isShopped;
        if (player.GetComponent<PlayerInputManager>().NPCtarget)
        {
            interactText.SetActive(true);
            if (Input.GetKeyDown(KeyCode.E))
            {
                ShopUI.SetActive(!ShopUI.activeSelf);


            }
        }
    }
}
