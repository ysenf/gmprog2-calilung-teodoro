﻿using UnityEngine;
using System.Collections;

public class SkillManager : MonoBehaviour {
	public GameObject skillTable;
    public Skill[] mySkills;
    public Skill currentSkill;
    public Actor me;
    public PlayerStateController myHead;
    public PlayerTargeting myArm;
    public bool isMonster;

	void Start () {
        Init();
	}
	
	void Update () {
        SecondUpdate();
	}

    protected virtual void Init()
    {
        me = gameObject.GetComponent<Actor>();
        myHead = me.GetComponent<PlayerStateController>();
        myArm = me.GetComponent<PlayerTargeting>();
        skillTable.SetActive(true);
        skillTable.SetActive(false);
        mySkills = new Skill[5];
        mySkills[0] = new GroundCrush(4f, 2);
        mySkills[1] = new Heal(5f, 8, 0f, this.gameObject);
        mySkills[2] = new Blessing(System.Type.GetType("STRINTBUFF"), 8, 2f);
        mySkills[3] = new Fireball(8f, 20, 0f, null, this.gameObject, 1);
    }

    public void SwapSkill(Skill skill)
    {
        currentSkill = skill;
    }

    public virtual void RunSkill()
    {
        myArm.ClearTarget();
        myHead.dontIdleMe = true;
        me.currMP -= currentSkill.skillCost;
        currentSkill.SkillExecute();
    }

    public void SkillAnimationTrigger()
    {
        currentSkill.SkillTrigger();
    }

    protected virtual void SecondUpdate()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            skillTable.SetActive(!skillTable.activeSelf);
        }
    }

    public Skill ReturnSkill(int ID)
    {
        return mySkills[ID];
    }

    public void StartEffect()
    {

    }

    public GameObject ManualSpawn(GameObject box, Vector3 spawnPlace, Quaternion rot)
    {
       GameObject fire =  Instantiate(box, spawnPlace, rot) as GameObject;
        return fire;
    }
}
