﻿using UnityEngine;
using System.Collections;

public class MonsterBehavior : StateMachineBehaviour {

    protected virtual void MyTransition(Animator animator)
    {

    }

   protected void CheckAggro(Animator animator)
    {
        if (animator.GetBool("inRange"))
        {
            animator.SetTrigger("toChase");
        }
    }

    protected void CheckLeash(Animator animator)
    {
        if (!animator.GetBool("inRange"))
        {
            animator.SetTrigger("toReturn");
        }
    }

    protected void CheckAware(Animator animator)
    {
        if (animator.GetBool("inRange"))
        {
            animator.SetTrigger("toChase");
        }
        else
            animator.SetTrigger("toReturn");
    }

    public void CheckHurt(Animator animator)
    {
        if (animator.GetComponent<Actor>().currHP <= (Mathf.RoundToInt(animator.GetComponent<Actor>().maxHP * 0.4f)))
            animator.SetBool("isHurt", true);
        else
            animator.SetBool("isHurt", false);
    }
}
