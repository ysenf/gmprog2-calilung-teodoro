﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class SkillButton : MonoBehaviour, IPointerDownHandler {

    public Skill currentSkill;
	public GameObject player;
    public GameObject skillbook;

	// Use this for initialization
	void Start () {
       currentSkill = new Skill();
        
		currentSkill.isCooldown = false;
	}
	
	// Update is called once per frame
	void Update () {
        currentSkill.skillTime += Time.deltaTime;
		if (currentSkill.isCooldown) {
			gameObject.transform.GetChild (0).GetComponent<Image> ().fillAmount -= 1.0f / currentSkill.skillTime * Time.deltaTime;
		} 
		if ((currentSkill.skillTime >= currentSkill.skillCooldown && currentSkill.isCooldown)) {
			gameObject.transform.GetChild (0).GetComponent<Image> ().fillAmount = 1.0f;
			currentSkill.isCooldown = false;
			Debug.Log ("Finished Cooling Down");
		}
	
	}

	public void OnPointerDown (PointerEventData eventData){
		if (!currentSkill.isCooldown && player.GetComponent<Actor>().currMP>=currentSkill.skillCost) {
            currentSkill.skillTime = 0;
            //	player.GetComponent<Actor> ().useSkill (currentSkill.skillCost);
            player.GetComponent<SkillManager>().RunSkill();
			currentSkill.skillTime = 0f;
			currentSkill.isCooldown = true;
			Debug.Log ("Use Skill");
		}
	}

    public void changeSkill(Skill newSkill)
    {
        if (!currentSkill.isCooldown)
        {
            newSkill.myExecutor = player;
            currentSkill = newSkill;
            player.GetComponent<SkillManager>().SwapSkill(newSkill);
            this.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = newSkill.skillIcon;

        }
    }

    public void GetSkill(int skillid)
    {
        currentSkill = skillbook.GetComponent<SkillTable>().GiveSkill(skillid);
    }
}
