﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NPCQuestManager : MonoBehaviour {
    public int goldReward;
    public int expReward;
    public Monster.MonsterType theType;
    public string theName;
    public int howMany;

	public GameObject player;
	public GameObject interactText;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (Vector3.Distance (transform.position, player.transform.position) <= 5) {
			interactText.SetActive(true);
			if (Input.GetKeyDown (KeyCode.E)) {
				Interacted (player);


			}
		} else if(Vector3.Distance (transform.position, player.transform.position) > 5){
			interactText.SetActive (false);

		}*/

	}

    public void Interacted(GameObject target)
    {
		StartCoroutine (qAdded ());
        target.GetComponent<QuestManager>().AddQuest(new KillQuest(howMany, expReward,goldReward,theType,theName,target.GetComponent<QuestManager>()));


    }

	public IEnumerator qAdded(){
		interactText.transform.GetChild(0).GetComponent<Text> ().text = "Now go kill " + howMany + " " + theType + "s.";
		interactText.SetActive (true);
		yield return new WaitForSeconds (2);
		interactText.SetActive (false);
	}
}
