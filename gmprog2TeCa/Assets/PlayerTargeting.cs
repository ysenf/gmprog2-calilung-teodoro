﻿using UnityEngine;
using System.Collections;

public class PlayerTargeting : MonoBehaviour {

    public GameObject myTarget;
    private NavMeshAgent agent;
    private Weapon myWeapon;
    private PlayerStateController stateCtrl;
    private Collider myCollider;
    private bool tellCombat = false;
    private bool seeking;
    private bool attacking;
    public float attackStopDistance = 0.5f;
    public GameObject lastTarget;

	void Start () {
        attacking = false;
        seeking = false;
        myWeapon = gameObject.GetComponent<Weapon>();
        stateCtrl = gameObject.GetComponent<PlayerStateController>();
        agent = gameObject.GetComponent<NavMeshAgent>();
        myTarget = null;
        myCollider = gameObject.GetComponent<Collider>();
	}
	

	void Update () {
        if (seeking)
        {
            agent.SetDestination(myTarget.transform.position);
            updateDistance();
        }
	}

    public void SetTarget(GameObject target)
    {
        if (!attacking)
        {
            myTarget = target;
            lastTarget = myTarget;
            myWeapon.SetTarget(myTarget);
            stateCtrl.inCombat = true;
            if (!CheckDistance())
            {
                seeking = true;
                agent.SetDestination(target.transform.position);
                agent.Resume();
            }
            else
            {
                seeking = false;
                agent.ResetPath();
                NotifyAttackState();
            }
        }

    }

    public void ClearTarget()
    {
        agent.ResetPath();
        myTarget = null;
        myWeapon.ClearTarget();
       // if (stateCtrl.inCombat || stateCtrl.canAttack)
      //  {
            stateCtrl.inCombat = false;
            stateCtrl.canAttack = false;
            stateCtrl.UpdateCombatStates();
            attacking = false;
      //  }
      //  else
      //  {
       //     stateCtrl.inCombat = false;
       //     stateCtrl.canAttack = false;
       // }
    }

    void updateDistance()
    {
        if (myTarget)
        {
            if (CheckDistance())
            {
                seeking = false;
                agent.ResetPath();
                NotifyAttackState();
            }
            else
                ResetAttackState();
        }
    }

    bool CheckDistance()
    {
        Vector3 closestPoint = myTarget.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        float distance = Vector3.Distance(closestPoint, transform.position);
        if (distance <= attackStopDistance)
            return true;
        else
            return false;
    }

    void NotifyAttackState()
    {
        Debug.Log("attacking");
        if (!stateCtrl.inCombat || !stateCtrl.canAttack)
        {
            stateCtrl.inCombat = true;
            stateCtrl.canAttack = true;
            stateCtrl.UpdateCombatStates();
        }
        else {
            stateCtrl.inCombat = true;
            stateCtrl.canAttack = true;
            stateCtrl.UpdateCombatStates();
        }
    }

    void ResetAttackState()
    {
        Debug.Log("chasing");
        if(!stateCtrl.inCombat || stateCtrl.canAttack)
        {
            stateCtrl.inCombat = true;
            stateCtrl.canAttack = false;
            stateCtrl.UpdateCombatStates();
        }
        else
        {
            stateCtrl.inCombat = true;
            stateCtrl.canAttack = false;
        }
    }

    public void AttackInvalid()
    {
        attacking = true;
    }

    public void AttackValid()
    {
        attacking = false;
    }

    public void OnDeathTargUpdate(GameObject victim)
    {
        if (myTarget== victim)
        {
            ClearTarget();
        }
        if (lastTarget == victim)
        {
            lastTarget = null;
        }
    }

}
