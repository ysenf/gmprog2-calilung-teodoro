﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class PlayerInputManager : MonoBehaviour
{
    private PlayerTargeting myHands;
    private PlayerNavigation myFeet;
    private PlayerStateController myHead;
    private GameObject targetMon;
    private Vector3 destination;
    private bool attacking;
    public GameObject NPCtarget;
    public EventSystem eventSystem;
	public GameObject interactText;

    void Start()
    {
        NPCtarget = null;
        myHead = gameObject.GetComponent<PlayerStateController>();
        myHands = gameObject.GetComponent<PlayerTargeting>();
        myFeet = gameObject.GetComponent<PlayerNavigation>();
        targetMon = null;
        destination = Vector3.zero;
    }

    void Update()
    {
        KeyInputRelay();
        MouseInputRelay();

		if (NPCtarget != null) {
			interactText.SetActive (true);
			
		} else if (NPCtarget == null) {
			interactText.SetActive (false);
		}
    }

    void KeyInputRelay()
    {
        if (Input.GetKeyDown(KeyCode.E)&&NPCtarget!=null)
        {
            NPCtarget.SendMessage("Interacted", this.gameObject);
        }
    }

    void MouseInputRelay()
    {
        if (!attacking)
        {
            RaycastHit hit;
            if (Input.GetMouseButtonDown(0) && !eventSystem.IsPointerOverGameObject())
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 99999f))
                {
                    if (hit.collider.gameObject.tag != "Foe")
                    {
                        MovementFunction(hit);
                    }
                    else
                    {
                        TargetFunction(hit);
                    }
                }

            }
        }
    }

    void MovementFunction(RaycastHit hit)
    {
        if(myHands.myTarget!=null)
        myHands.ClearTarget();
        myFeet.ReceiveInputSetPath(hit);
    }

    void TargetFunction(RaycastHit hit)
    {
        transform.LookAt(hit.collider.gameObject.transform);
        myFeet.ClearTarget();
        myHands.SetTarget(hit.collider.gameObject);
    }

    public void AttackStart()
    {
        myHands.AttackInvalid();
        attacking = true;
    }

    public void AttackEnd()
    {
        // myHead.dontIdleMe = false;
        Debug.Log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        myHead.setWait();
        myHands.AttackValid();
        attacking = false;
    }

    public void SetNPCTarget(GameObject NPC)
    {
        NPCtarget = NPC;
    }
}
