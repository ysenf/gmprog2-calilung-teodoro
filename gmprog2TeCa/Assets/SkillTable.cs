﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillTable : MonoBehaviour
{
    public SkillManager myBook;
    public GameObject slot;
    public List<GameObject> slotTable = new List<GameObject>();
    public List<Skill> skillTable = new List<Skill>();
    public int windowX;
    public int windowY;
    public GameObject skillButton;
    public int skillCap;

    // Use this for initialization
    void Awake()
    {
        this.gameObject.SetActive(true);
        int slotAmt = 0;

        for (int i = 0; i < 4; i++)
        {
            GameObject slots = (GameObject)Instantiate(slot);
            slots.GetComponent<slotSkillManager>().slotNumber = slotAmt;
            slotAmt++;
            slotTable.Add(slots);
            skillTable.Add(new Skill());
            slots.transform.parent = this.gameObject.transform;
            slots.name = "Slot" + (i + 1);
            slots.GetComponent<RectTransform>().localPosition = new Vector3(windowX, windowY, 0);
            windowY -= 70;
            myBook = GameObject.FindGameObjectWithTag("Player").GetComponent<SkillManager>();
           // skillTable[0] = myBook.mySkills[0];
          //  if (skillButton.GetComponent<SkillButton>().currentSkill == null)
         //   {
         //       changeSkill(skillTable[0]);
        //    }
        }

        //skillTable[0] = myBook.mySkills[0];
        //skillTable[0] = new GroundCrush();
        /* skillTable [1] = mySkills[1].GetComponent<Skill>();
         skillTable [2] = mySkills[2].GetComponent<Skill>();
         skillTable [3] = mySkills[3].GetComponent<Skill>();*/

        this.gameObject.SetActive(true);
        this.gameObject.SetActive(false);

    }

    void Start()
    {


        skillTable[0] = myBook.mySkills[0];
        if (skillButton.GetComponent<SkillButton>().currentSkill == null)
        {
            changeSkill(skillTable[0]);
        }
    
    // myBook = GameObject.FindGameObjectWithTag("Player").GetComponent<SkillManager>();
    // if (skillButton.GetComponent<SkillButton>().currentSkill == null)
    //  {
    //      changeSkill(skillTable[0]);
    //  }
}

    // Update is called once per frame
    void Update()
    {
        //  if (skillButton.GetComponent<SkillButton>().currentSkill == null)
        //  {
        //      changeSkill(skillTable[0]);
        //  }
    }

    public void changeSkill(Skill skill)
    {
        Debug.Log("changed");
        skillButton.GetComponent<SkillButton>().changeSkill(skill);
    }

    public Skill GiveSkill(int skillid)
    {
        Debug.Log("wtf");
        return myBook.mySkills[skillid];
    }

    public void LearnSkill(int level)
    {
        if (level > skillCap)
        {
            level = skillCap;
        }
        skillTable[level - 1] = myBook.mySkills[level-1];
    }
}
