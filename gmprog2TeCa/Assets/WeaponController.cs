﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

    public bool isHitting;
    public bool isPlayer;
    private Weapon myWep;
    public GameObject parent;
    public GameObject target;

	void Start () {
        isHitting = false;
        ArmWeapon(parent);
	}
	

	void Update () {
        myWep.isHitting = isHitting;
	}

    void OnTriggerEnter (Collider other)
    {
        if (isPlayer)
        {
            if(other.CompareTag("Foe") && other.gameObject == target)
            {
                isHitting = true;
            }
        }
        else
        {
            if (other.CompareTag("Player"))
            {
                isHitting = true;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (isPlayer)
        {
            if (other.CompareTag("Foe") && other.gameObject == target)
            {
                Debug.Log("staying");
                isHitting = true;
            }
        }
        else
        {
            if (other.CompareTag("Player"))
            {
                isHitting = true;
            }
        }
    }

    public void ResetHitbox()
    {
        Debug.Log("#############################################################################");
        isHitting = false;
    }

    public void ArmWeapon(GameObject arm)
    {
        myWep = arm.gameObject.GetComponent<Weapon>();
    }
}
