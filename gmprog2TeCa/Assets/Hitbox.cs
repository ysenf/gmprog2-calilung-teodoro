﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hitbox : MonoBehaviour {
    public string targetType;
    public int damage;
    public List<GameObject> affected;
    public GameObject myParent;
    public float decay;

    void Start()
    {
        Destroy(this.gameObject, decay);
    }

    void OnTriggerEnter(Collider other)
    {
        if (!affected.Contains(other.gameObject)&&other.CompareTag(targetType))
        {
            other.GetComponent<Actor>().TakeDamage(damage, myParent);
            affected.Add(other.gameObject);
        }
    }
}
