﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour {
    public GameObject interactText;
	public GameObject questLog;
    public List<KillQuest> myQuests;

    // Use this for initialization
    void Start () {
        myQuests = new List<KillQuest>();
        Debug.Log(myQuests.Count);
	}
	
	// Update is called once per frame
	void Update () {
         /* int i = 0;
          while (i < myQuests.Count)
          {
              Debug.Log(myQuests[i].GiveProgress());
              i++;
          }*/

        Debug.Log(myQuests.Count);

		if (Input.GetKeyDown (KeyCode.Q)) {
			questLog.SetActive (!questLog.activeSelf);
		}
		//UpdateQuests ();

    }

    public void AddQuest(KillQuest toAdd)
    {
        myQuests.Add(toAdd);
        UpdateQuests();
    }

    public void RemoveQuest(KillQuest toRemove)
    {
        myQuests.Remove(toRemove);
        UpdateQuests();
    }

    public void UpdateKillQuest(Monster.MonsterType typeKilled)
    {
        int i = 0;
        while (i < myQuests.Count)
        {
            myQuests[i].UpdateObjective(typeKilled);
            i++;
        }
        Debug.Log("testtetst");
       CombQuests();
       UpdateQuests();
    }

    public void CombQuests()
    {
        Debug.Log("eh");
        int i = 0;
        while (i < myQuests.Count)
        {
            if (myQuests[i].CheckProgress())
            {
                myQuests[i].Reward();
                myQuests[i].RemoveMe();
                i--;
            }
            i++;
        }
    }

    public void UpdateQuests()
    {
       // Debug.Log("wekwkeke");
        int x = 0;
        while (x <= 7)
        {
            questLog.transform.GetChild(0).GetChild(x).GetComponent<Text>().text = null;
            questLog.transform.GetChild(0).GetChild(x).GetChild(0).GetComponent<Text>().text = null;
            x++;
        }
		int i = 0;
        //ALL THE DATA YOU NEED IS HERE
        foreach(KillQuest quest in myQuests)
        {
			questLog.transform.GetChild (0).GetChild(i).GetComponent<Text> ().text = quest.MyName;
			questLog.transform.GetChild (0).GetChild (i).GetChild(0).GetComponent<Text> ().text = quest.GiveProgress ();
            //THESE ARE THE ONLY THINGS YOU NEED TO PRINT
            //IN THE QUEST TRACKER WINDOW
            //QUEST NAME AND QUEST PROGRESS. THE PROGRESS IS ALREADY GIVEN AS A WHOLE STRING, THE METHOD RETURNS IT (IT READS AS "KILLED X OUT OF Y").
            //vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            //quest.GiveProgress();    <------- returns a string
            //quest.MyName;
			i++;
        }
    }
}
